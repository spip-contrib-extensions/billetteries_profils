<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ajout du champ de profil sur les types de billets
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function billetteries_profils_declarer_tables_objets_sql($tables) {
	$tables['spip_billets_types']['field']['id_profil'] = 'bigint(21) NOT NULL DEFAULT 0';
	$tables['spip_billets_types']['champs_editables'][] = 'id_profil';
	$tables['spip_billets_types']['champs_versionnes'][] = 'id_profil';
	
	return $tables;
}

/**
 * Modifier la description des saisies d'un formulaire
 *
 * - Configuration des billetteries : options de profil
 * - Édition d'un type de billet : ajout du champ profil
 *
 * @param [type] $flux
 * @return void
 */
function billetteries_profils_formulaire_saisies($flux) {
	// Ajouter une configuration
	if ($flux['args']['form'] == 'configurer_billetteries') {
		include_spip('inc/saisies');
		
		$flux['data'][] = array(
			'saisie' => 'case',
			'options' => array(
				'nom' => 'forcer_profil',
				'label_case' => _T('billetteries:cfg_forcer_profil_label_case'),
				'defaut' => lire_config('billetteries/forcer_profil'),
			),
		);
	}
	// Ajouter le champ de profil pour les types de billets
	elseif ($flux['args']['form'] == 'editer_billets_type') {
		include_spip('inc/saisies');
		include_spip('inc/config');
		
		$saisie_profil = array(
			'saisie' => 'profils',
			'options' => array(
				'nom' => 'id_profil',
				'label' => _T('billets_type:champ_id_profil_label'),
				'explication' => _T('billets_type:champ_id_profil_explication'),
				'option_intro' => _T('profil:info_aucun_profil'),
				'obligatoire' => lire_config('billetteries/forcer_profil'),
				'cacher_option_intro' => lire_config('billetteries/forcer_profil'),
				'defaut' => lire_config('billetteries/forcer_profil') ? lire_config('profils/id_profil_defaut') : null,
			),
		);
		$chemin_taxe = saisies_chercher($flux['data'], 'quota', true);
		$flux['data'] = saisies_inserer($flux['data'], $saisie_profil, $chemin_taxe);
	}
	
	return $flux;
}

/**
 * Modifier le résultat de la compilation des squelettes
 *
 * - Liste des types de billets du privé : ajout d'un lien vers le profil
 *
 * @param array $flux
 * @return array
 */
function billetteries_profils_recuperer_fond($flux) {
	if (
		$flux['args']['fond'] == 'prive/objets/contenu/billets_type'
		and $id_billets_type = $flux['args']['contexte']['id_billets_type']
	) {
		if ($id_profil = sql_getfetsel('id_profil', 'spip_billets_types', 'id_billets_type = '.$id_billets_type)) {
			include_spip('inc/texte');
			$titre_profil = '<a href="'.generer_url_entite($id_profil, 'profil').'">' . typo(sql_getfetsel('titre', 'spip_profils', 'id_profil = '.$id_profil)) . '</a>';
		}
		else {
			$titre_profil = _T('profil:info_aucun_profil');
		}
		
		$profil = '
<div class="champ contenu_id_profil">
	<div class="label">' . _T('billets_type:champ_id_profil_label') . ' : </div>
	<span class="id_profil">' . $titre_profil . '</span>
</div>';

		$flux['data']['texte'] = $profil . $flux['data']['texte'];
	}
	
	return $flux;
}

/**
 * Compléter le chargement des valeurs des formulaires
 *
 * Formulaire de profil : si c'est pour éditer un billet,
 * on prend en priorité les infos du billet, qui ne sont pas nécessairement les mêmes que celles du profil.
 *
 * @param array $flux
 * @return array
 */
function billetteries_profils_formulaire_charger($flux) {
	
	if (
		$flux['args']['form'] == 'profil'
		and isset($flux['args']['args'][3]['id_billet'])
	) {
		$options = $flux['args']['args'][3];
		$id_billet = $options['id_billet'];
		$infos = sql_getfetsel('infos', 'spip_billets', 'id_billet='.intval($id_billet));
		$infos = (is_array(unserialize($infos)) ? unserialize($infos) : array());
		if ($infos_profil = (isset($infos['profil']) ? $infos['profil'] : array())) {
			$flux['data'] = array_merge($flux['data'], $infos_profil);
		}
	}

	return $flux;
}

/**
 * Compléter les traitements des formulaires
 *
 * Auto-complétion : si on édite un billet au moyen du formulaire de profil,
 * mettre à jour les informations de profil dans les infos du billet.
 *
 * @param array $flux
 * @return array
 */
function billetteries_profils_formulaire_traiter($flux) {

	$form = (isset($flux['args']['form']) ? $flux['args']['form'] : null);

	// Formulaire de profil : s'il sert à éditer un billet.
	if (
		$form === 'profil'
		and isset($flux['args']['args'][3]['id_billet']) // il y a id_billet dans les options
	) {
		$id_billet = intval($flux['args']['args'][3]['id_billet']);
		$id_auteur = intval($flux['data']['id_auteur']); // optionnel : vide si on prend un billet pour un autre
		$email     = $flux['data']['email_principal'];
		$profil    = $flux['data']['infos']; // les infos de profil arrivent déjà toutes cuites, identiques à profils_profil_lire()
		$set = array(
			'id_auteur' => $id_auteur,
			'email'     => $email,
		);
		// S'il y a des infos de profil à ajouter au billet
		if (
			$id_billet
			and $profil
		) {
			include_spip('base/abstract_sql');
			// On ajoute le profil aux infos sérialisées
			$infos = array();
			$infos_serialisees = sql_getfetsel('infos', 'spip_billets', "id_billet=$id_billet");
			$infos_serialisees = ($infos_serialisees ? $infos_serialisees : '');
			try {
				$infos = unserialize($infos_serialisees);
			} catch (Exception $e) {
				$erreur = $e->getMessage();
				spip_log("[formulaire_traiter] échec unserialize des infos du billet $id_billet : $erreur", 'billetteries_profils' . _LOG_ERREUR);
			}
			$infos['profil'] = $profil;
			try {
				$infos_serialisees = serialize($infos);
			} catch (Exception $e) {
				$erreur = $e->getMessage();
				spip_log("[formulaire_traiter] échec serialize des infos du billet $id_billet : $erreur", 'billetteries_profils' . _LOG_ERREUR);
			}
			$set['infos'] = $infos_serialisees;
			// API SQL au lieu de objet_modifier pour ne pas mettre à jour le profil 2 fois d'affilée
			// en passant dans post_edition → billet_completer_champs
			sql_updateq('spip_billets', $set, "id_billet = $id_billet");
			spip_log("[formulaire_traiter] mise à jour OK du profil dans les infos du billet $id_billet", 'billetteries_profils' . _LOG_DEBUG);
		}
	}

	return $flux;
}

/**
 * Compléter les champs d'un billet après son édition
 *
 * On ajoute le profil dans le champ sérialisé `infos` si on a un id_auteur
 *
 * @param array $flux
 * @return array
 */
function billetteries_profils_billet_completer_champs($flux) {

	$id_billet = intval($flux['args']['id_billet']);
	$billet = $flux['args']['billet'];
	$id_auteur = intval(!empty($flux['data']['id_auteur']) ? $flux['data']['id_auteur'] : $billet['id_auteur']);
	$id_billets_type = intval(!empty($flux['data']['id_billets_type']) ? $flux['data']['id_billets_type'] : $billet['id_billets_type']);
	$infos = !empty($flux['data']['infos']) ? $flux['data']['infos'] : $billet['infos'];

	if (
		// Si on a un id_auteur
		$id_auteur
		// et un profil associé au type de billet
		and $id_billets_type > 0
		and $id_profil = intval(sql_getfetsel('id_profil', 'spip_billets_types', "id_billets_type = $id_billets_type"))
	) {
		include_spip('inc/profils');
		$profil = profils_profil_lire($id_auteur, $id_profil, ['format' => 'complet', 'filtrer_vides' => true]);
		$infos_array = array();
		try {
			$infos_array = unserialize($infos);
		} catch (Exception $e) {
			$erreur = $e->getMessage();
			spip_log("[billet_completer_champs] échec unserialize infos du billet $id_billet : $erreur", 'billetteries_profils' . _LOG_ERREUR);
		}
		$infos_array = (is_array($infos_array) ? $infos_array : array());
		$infos_array['profil'] = $profil;
		$infos = serialize($infos_array);
		$flux['data']['infos'] = $infos;
		spip_log("[billet_completer_champs] mise à jour OK du profil dans les infos du billet $id_billet", 'billetteries_profils' . _LOG_DEBUG);
	}

	return $flux;
}

/**
 * Optimiser la base de données
 *
 * Mettre à la poubelle les comptes visiteurs qui ne sont plus reliés à aucun billet
 *
 * @pipeline optimiser_base_disparus
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function billetteries_profils_optimiser_base_disparus($flux) {
	// En pause pour l'instant car difficile de savoir ce qui peut être légitime ou pas.
	// Si on installe ce plugin générique sur un site où on peut aussi avoir un compte utilisateur pour d'autres choses.

	return $flux;
}

/**
 * Ajouter des colonnes du profil à l'export des billets
 *
 * @pipeline billetteries_exporter_billets_colonnes
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function billetteries_profils_billetteries_exporter_billets_colonnes($flux) {

	if (
		isset($flux['args']['billets_type']['id_profil'])
		and $id_profil = intval($flux['args']['billets_type']['id_profil'])
	) {
		// On prend un auteur fictif pour avoir les colonnes correspondant à l'édition
		// On prend le format label, idem que les colonnes déjà présentes
		include_spip('inc/profils');
		$ligne_profil = profils_recuperer_infos_simples(-1, $id_profil, array('format_cles' => 'label'));
		$colonnes_profil = array_keys($ligne_profil);
		$flux['data'] = array_merge($flux['data'], $colonnes_profil);
	}

	return $flux;
}

/**
 * Ajouter des données à l'export des billets
 *
 * @pipeline billetteries_exporter_billets_ligne
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function billetteries_profils_billetteries_exporter_billets_ligne($flux) {
	if (
		isset($flux['args']['billets_type']['id_profil'])
		and $id_profil = intval($flux['args']['billets_type']['id_profil'])
	) {
		// En priorité, on prend les infos de profil présentes dans le champ infos
		if (
			isset($flux['args']['billet']['infos'])
			and $infos = unserialize($flux['args']['billet']['infos'])
			and !empty($infos['profil'])
		) {
			$infos_profil = $infos['profil'];
			// On applati le tableau multidimensionnel
			$data = array();
			$iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($infos_profil));
			foreach($iterator as $v) {
				$flux['data'][] = $v;
			}
		// Sinon on prend le profil actuel de l'auteur
		} elseif (
			isset($flux['args']['billet']['id_auteur'])
			and $id_auteur = intval($flux['args']['billet']['id_auteur'])
		) {
			include_spip('inc/profils');
			$ligne = profils_recuperer_infos_simples($id_auteur, $id_profil);
			$flux['data'] += $ligne;
		}
	}
	
	return $flux;
}

/**
 * Modifier l'affichage du champ `infos` d'un billet
 *
 * On affiche séparément les infos du profil
 *
 * @param array $flux
 * @return array
 */
function billetteries_profils_billet_afficher_infos($flux) {
	$infos = $flux['args']['infos'];
	$id_billet = $flux['args']['id_billet'];
	$id_billets_type = $flux['args']['id_billets_type'];
	$id_auteur = $flux['args']['id_auteur'];
	if (isset($infos['profil'])) {
		// D'abord les champs du profil
		include_spip('inc/profils');
		$id_profil = sql_getfetsel('id_profil', 'spip_billets_types', 'id_billets_type='.intval($id_billets_type));
		$infos_profil = $infos['profil'];
		$saisies = profils_chercher_saisies_profil('edition', $id_auteur, $id_profil);
		$contexte = array(
			'saisies' => $saisies,
			'valeurs' => $infos_profil,
		);
		$champs_profil = recuperer_fond('inclure/voir_saisies', $contexte);
		$champs_profil = str_replace('champ afficher', 'champ afficher contenu_infos contenu_infos_profil', $champs_profil);
		$texte = $champs_profil;
		// Puis les autres champs "non gérés", avec des labels techniques à priori
		unset($infos['profil']);
		$champs_autres = recuperer_fond('prive/squelettes/inclure/billet_champs_infos', array('infos' => $infos));
		$texte .= $champs_autres;

		$flux['data'] = $texte;
	}

	return $flux;
}
