# Billetteries nominatives par Profils

Ce plugin fait l’interface entre le plugin [Billetteries](https://git.spip.net/spip-contrib-extensions/billetteries) et le plugin [Profils](https://git.spip.net/spip-contrib-extensions/profils).

Il ajoute la possibilité d’éditer des billets au moyen du plugin Profils, et conserver ainsi dans chaque billet des informations de contact complètes.

## Pourquoi ?

Par défaut, un billet ne conserve que des informations minimales sur la personne qui possède le billet : au moins son email, et éventuellement son numéro de compte (`id_auteur`).

Mais cela pose 2 limites :
* Une personne peut avoir des billets sans avoir de compte sur le site, dans ce cas on ne connaît que son email.
* Si elle a un compte utilisateur, ses informations de contact peuvent changer au cours du temps : changement d'adresse, d'email, etc. Hors on souhaite connaître les informations de contact à un instant T, *au moment où le billet est commandé*.

C'est pour cela qu'un champ `infos` est prévu, il permet d'y mettre toutes les infos complémentaires que l'on juge utiles sous forme sérialisée, dont des informations de contact.

Ce plugin automatise tout cela, les champ `infos` est complété automatiquement avec les informations d’un profil, dans une clé `profil`.

## Utilisation

Commencez par créer un ou plusieurs profils qui seront utilisés pour éditer les billets.

Attribuez ensuite un profil à chaque type de billet :
![](docs/edit_billet_type_profil.webp)

### Dans vos squelettes

Dans votre tunnel de commande, une fois les billets créés, par exemple après la mise au panier au moyen du formulaire `#FORMULAIRE_COMMANDER_BILLETTERIE`, vous pouvez utiliser le formulaire de profil `#FORMULAIRE_PROFIL` pour éditer chaque billet.

Il suffit pour cela d'ajouter le numéro de billet dans les options du formulaire.

Dans cet exemple simplifié :
* On n'édite que le 1er billet mis au panier.
* On attribue le billet à la personne connectée.

Dans la vraie vie, il faut bien sûr faire en sorte d'éditer tous les billets au panier, séquentiellement ou non.
De plus il vous appartient de choisir s’il faut lier chaque billet à un compte ou non, c'est optionnel. Ici on met `#SESSION{id_auteur}` pour faire simple.

```html
<BOUCLE_billets(BILLETS) {panier_visiteur} {tout} {0,1} {!par date}>
[(#FORMULAIRE_PROFIL{
	#SESSION{id_auteur},
	#INFO_ID_PROFIL{billets_type, #ID_BILLETS_TYPE},
	#URL_PAGE{xxx},
	#ARRAY{
		id_billet,#ID_BILLET,
	}
})]
</BOUCLE_billets>
```

### Voir et utiliser les informations de contact

Dans l’espace privé, ces informations de contact seront visibles dans le champ `Informations diverses` :

![](docs/preview_infos.webp)

Elles sont enregistrées sous forme de tableau dans la clé `profil` du champ `infos` (qui est sérialisé).
Le tableau est au format du plugin Profils.

Si vous souhaitez exploiter ou afficher ces informations, un exemple dans `billetteries_profils_billet_afficher_infos()`.


## Limites / todo

À l'heure actuelle, il n’y a pas moyen d’éditer un billet de cette façon dans l’espace privé, pour inscrire une personne à sa place par exemple.

Autrement dit, si vous créez un billet dans l’espace privé, les informations de contact ne seront pas remplies.

Le formulaire standard d'édition de billet n’est pas branché avec ce plugin, c'est une évolution possible.
