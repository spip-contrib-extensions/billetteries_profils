# Changelog

## 1.2.0 - 2023-06-28

### Added

* Ajout d'un readme
* Ajout d'un changelog
* Auto remplir les infos de profil avec le formulaire editer_billet